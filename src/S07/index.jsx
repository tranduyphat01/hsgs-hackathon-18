import React from "react";
import "./index.css";

debugger
class Cell extends React.Component {
    render() {
        const array = [];
        console.log("Props = " + this.props);
        let size = this.props.playField.length;
        for (let i = 0; i < size; i++) {
            const col = [];
            for (let j = 0; j < size; j++) {
                if (this.props.playField[i][j] === -1) {
                    col.push(<td key={i * size + j}>X</td>)
                }
                else {
                    col.push(<td key={i * size + j}>{this.props.playField[i][j]}</td>);
                }
            }
            array.push(<tr>{col}</tr>)
        }
        console.log(array);

        return <tr>{array}</tr>;
    }
}

class Board extends React.Component {
    render() {
        console.log(this.props);
        console.log(this.props.size);
        console.log(this.props.state.playField);
        return (
            <div>
                <table style={{border: "solid black"}}>
                    <tbody>
                    <Cell playField={this.props.state.playField}/>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Board;
